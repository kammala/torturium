Description
=====

Little Python application to test WebSocket in Tornado web framework.

Requirements
=====
This project has been wroten on Python 3 and use some syntax that is not supported by Python 2(at least, metaclasses).
Required python libs are placed in _requirements.txt_.

Installation and quick start
=====
Get the code
```
git clone https://kamaevpp@bitbucket.org/kamaevpp/torturium.git
```
Install the requirements with ```pip```:
```
cd torturium
## if you want use virtualenv
# virtualenv -p python3 .venv
# .venv/bin/activate
pip install -r requirements.txt
```
And run the server:
```
python manage.py
```
For run options try:
```
python manage.py --help
```
Test your installation at [localhost](http://127.0.0.1/) — there will be simple web ui.
