import logging
import datetime as dt

logger = logging.getLogger(__name__)
import uuid
import json

from tornado.websocket import WebSocketHandler

from torturium.message import parse, Login, Join, Leave, Exit, Write


class WebSocket(WebSocketHandler):
    opened_connections = {}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.conn_id = uuid.uuid4()
        self.user = None
        self.subscribes = set()

    def check_origin(self, origin):
        return True

    def open(self, *args, **kwargs):
        self.opened_connections[self.conn_id] = self
        logger.debug('%s : Opened WS connection', self.conn_id)

    def on_close(self):
        del self.opened_connections[self.conn_id]
        logger.debug('%s : Closed WS connection', self.conn_id)

    def on_message(self, message):
        logger.debug('%(conn_id)s : Get message: %(message)s', {'message': message, 'conn_id': self.conn_id})
        msg = parse(message)
        if msg.is_valid():
            extra_data = {}
            try:
                if isinstance(msg, Exit):
                    self.close()
                elif isinstance(msg, Login):
                    self.user = msg.nick
                    extra_data['user'] = self.user
                elif isinstance(msg, Join):
                    self.subscribe(msg.room)
                elif isinstance(msg, Leave):
                    self.unsubscribe(msg.room)
                elif isinstance(msg, Write):
                    self.publish(msg.room, msg.body)
            except RuntimeError as ex:
                self.write_message(json.dumps({'status': 'failed', 'errors': [str(ex)]}))
            else:
                self.write_message(json.dumps(dict(status='ok', command=msg.__class__.__name__, **extra_data)))
        else:
            self.write_message(json.dumps({'status': 'failed', 'errors': msg.errors}))

    def publish(self, room, body):
        if not self.user:
            raise RuntimeError('You cannot write messages without login')
        for conn in self.opened_connections.values():
            if room in conn.subscribes:
                conn.write_message(json.dumps({'action': 'get_message',
                                               'from': self.user, 'msg': body, 'room': room,
                                               'server_ts': dt.datetime.now().isoformat()}))

    def subscribe(self, room):
        self.subscribes.add(room)

    def unsubscribe(self, room):
        if room in self.subscribes:
            self.subscribes.remove(room)
        else:
            logger.warn('No such room in subscribes')
