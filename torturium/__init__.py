import logging
import os
from torturium.webui import WebUI

logger = logging.getLogger(__name__)

from tornado.ioloop import IOLoop
from tornado.web import Application, url

from .ws import WebSocket


def make_app():
    root_path = os.path.dirname(__file__)
    return Application([
        url(r"/ws/", WebSocket, name='ws'),
        url(r"/", WebUI, name='index'),
    ], template_path=os.path.join(root_path, 'templates'), static_path=os.path.join(root_path, 'static'))


def main(host='localhost', port='8888'):
    logger.info('Running app at {}:{}'.format(host, port))
    app = make_app()
    app.listen(port, host)
    try:
        IOLoop.instance().start()
    except KeyboardInterrupt:
        IOLoop.instance().stop()

